﻿using System;

namespace vending_machine
{
    public class CashStack : IMoneyCollection
    {
        private Cash[] array = new Cash[Enum.GetValues(typeof(Cash.EType)).Length];
        /// <summary> The total value of the money within the stack. </summary>
        public double Value
        {
            get
            {
                float amount = 0f;
                foreach (Cash cash in array)
                {
                    amount += cash.Value * cash.Quantity;
                }

                return amount;
            }
        }
        private double CoinValue
        {
            get
            {
                float amount = 0f;
                foreach (Cash cash in array)
                {
                    if (cash.IsCoin)
                    {
                        amount += cash.Value * cash.Quantity;
                    }
                    else
                    {
                        break;
                    }
                }

                return amount;
            }
        }

        public CashStack() : this(0)
        {
        }
        public CashStack(double value)
        {
            // Initialize the array.
            int index = 0;
            foreach (Cash.EType cashType in Enum.GetValues(typeof(Cash.EType)))
            {
                array[index++] = new Cash(cashType);
            }

            // Set the total value of the stack to the parameter
            double currValue = 0f;
            for (int i = array.Length - 1; i >= 0; i--)
            {
                while (currValue + array[i].Value <= value)
                {
                    array[i].Quantity++;
                    currValue += array[i].Value;
                }
            }
        }

        /// <summary> Insert a coin / banknote to the cash stack. </summary>
        /// <param name="unitValue"> The value of the coin / banknote. </param>
        /// <returns> True if the value exists within the known coins / banknotes and it was added to the stack, false otherwise. </returns>
        public bool Insert(double unitValue)
        {
            Cash c = Array.Find(array, cash => cash.Value == unitValue);
            if (c != null)
            {
                c.Quantity++;
                return true;
            }

            return false;
        }

        /// <summary> Add a certain amount of money to the stack. </summary>
        /// <param name="value"> The amount of money you want to add. </param>
        public void Add(double value)
        {
            Add(value, false);
        }
        /// <param name="onlyCoins"> Should there be added only coins to the stack? </param>
        public void Add(double value, bool onlyCoins)
        {
            float addedValue = 0f;
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (onlyCoins && array[i].IsCoin == false)
                {
                    continue;
                }

                while (addedValue + array[i].Value <= value)
                {
                    array[i].Quantity++;
                    addedValue += array[i].Value;
                }
            }
        }


        /// <summary> Remove a certain amount of money from the stack. </summary>
        /// <param name="value"> The amount of money you want to remove. </param>
        /// <returns> True if there is enough money within the stack and it has been withdrawn, false otherwise. </returns>
        public bool Remove(double value)
        {
            return Remove(value, false);
        }
        /// <param name="onlyCoins"> Should there be removed only coins from the stack? </param>
        public bool Remove(double value, bool onlyCoins)
        {
            if (value > Value || (onlyCoins && value > CoinValue))
            {
                return false;
            }

            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (onlyCoins && array[i].IsCoin == false)
                {
                    continue;
                }

                while (array[i].Quantity > 0 && value > array[i].Value)
                {
                    array[i].Quantity--;
                    value -= array[i].Value;
                }
            }
            return true;
        }
    }
}
