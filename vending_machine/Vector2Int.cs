﻿namespace vending_machine
{
    public class Vector2Int
    {
        public int x;
        public int y;

        public Vector2Int() : this(0, 0)
        {
        }
        public Vector2Int(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
