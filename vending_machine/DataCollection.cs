﻿using System;
using System.Collections.Generic;
using System.IO;

namespace vending_machine
{
    public class DataCollection
    {
        public class ProductSaleInfo : IEquatable<Product>
        {
            public string Name;
            public string Category;
            public byte Size;
            public uint Sold;

            public ProductSaleInfo() : this("Unknown", "Unknown", 0)
            {
            }
            public ProductSaleInfo(string name, string category, byte size)
            {
                Name = name;
                Category = category;
                Size = size;
                Sold = 1;
            }

            public bool Equals(Product product)
            {
                if(Name == product.Name && Category == product.Category.ToString() && Size == product.Size)
                {
                    return true;
                }

                return false;
            }
        }
        private HashSet<ProductSaleInfo> productSales;
        public uint TransactionsCount { get; private set; }
        public double Profit { get; private set; }

        private readonly string defaultFileName = "sales.txt";


        public DataCollection()
        {
            productSales = new HashSet<ProductSaleInfo>();
            InitializeFromFile(defaultFileName);
        }

        private void InitializeFromFile(string fileName)
        {
            string[] stages = { "Profit: ", "Transactions: ", "Product Sales: " };
            int stageIndex = 0;
            string[] lines = File.ReadAllLines(@fileName);
            foreach(string line in lines)
            {
                switch (stageIndex)
                {
                    case 0:
                        if (line.Contains(stages[stageIndex]))
                        {
                            Profit = Convert.ToDouble(line.Substring(stages[stageIndex].Length));
                            stageIndex++;
                        }
                        break;

                    case 1:
                        if (line.Contains(stages[stageIndex]))
                        {
                            TransactionsCount = Convert.ToUInt32(line.Substring(stages[stageIndex].Length));
                            stageIndex++;
                        }
                        break;

                    case 2:
                        if (line.Contains(stages[stageIndex]))
                        {
                            stageIndex++;
                        }
                        break;

                    case 3:
                        ProductSaleInfo auxSaleInfo = new ProductSaleInfo();
                        string[] words = line.Split(new char[]{ ' ', '\t' });
                        for(int i = 0; i < words.Length; i++)
                        {
                            switch(words[i])
                            {
                                case "Name:":
                                    auxSaleInfo.Name = words[++i];
                                    break;

                                case "Category:":
                                    auxSaleInfo.Category = words[++i];
                                    break;

                                case "Sized:":
                                    auxSaleInfo.Size = Convert.ToByte(words[++i]);
                                    break;
                                    
                                case "Sold:":
                                    auxSaleInfo.Sold = Convert.ToUInt32(words[++i]);
                                    break;
                            }
                        }

                        productSales.Add(auxSaleInfo);
                        break;
                }
            }
        }

        private void UpdateFile(string fileName)
        {
            File.WriteAllLines(@defaultFileName, contents: GetProductSalesInformation());
        }

        public void AddSoldProduct(Product product)
        {
            foreach(ProductSaleInfo info in productSales)
            {
                if(info.Equals(product))
                {
                    info.Sold++;
                    return;
                }
            }

            productSales.Add(new ProductSaleInfo(product.Name, product.Category.ToString(), product.Size));

            TransactionsCount++;
            Profit += product.Price;

            UpdateFile(defaultFileName);
        }

        // Change to IEnumerator?
        public string[] GetProductSalesInformation()
        {
            List<string> informationList = new List<string>();

            informationList.Add("Profit: " + Profit);
            informationList.Add("Transactions: " + TransactionsCount);
            informationList.Add(string.Empty);
            informationList.Add("Product Sales: ");

            foreach(ProductSaleInfo productInfo in productSales)
            {
                string information = string.Empty;
                information += "Name: " + productInfo.Name + "   ";
                information += "Category: " + productInfo.Category + "   ";
                information += "Sized: " + productInfo.Size + "   ";
                information += "Sold: " + productInfo.Sold;

                informationList.Add(information);
            }

            return informationList.ToArray();
        }
    }
}
