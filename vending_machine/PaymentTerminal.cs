﻿using System;
using System.Collections.Generic;

namespace vending_machine
{
    public class PaymentTerminal
    {
        // Observable
        public event EventHandler<string> ObserverEvent;

        public enum EPayMethod { CASH, CREDIT };
        public EPayMethod PayMethod { get; set; }
        public double AmountRequired { get; set; }
        public bool PaymentInProcess => AmountRequired > 0f ? true : false;


        public PaymentTerminal()
        {
        }
        
        /// <summary> Collect the amount of money inserted through the input terminal. </summary>
        public void CollectMoney(double amount)
        {
            AmountRequired -= amount;
            if(AmountRequired < 0f)
            {
                AmountRequired = 0f;
            }

            NotifyObservers(AmountRequired.ToString());
        }

        /// <summary> Get, as a string, the list of the currently available payment methods. </summary>
        public string GetPaymentMethodListInformation()
        {
            string information = string.Empty;
            int index = 0;
            foreach(EPayMethod method in Enum.GetValues(typeof(EPayMethod)))
            {
                information += (index < 10 ? " " : "") + index++ + ". ";
                information += method;
                information += "\n";
            }
            return information;
        }

        /// <summary> Check wether the credit card is valid for payment or not. </summary>
        /// <param name="creditCard"> The credit card you wish to validate. </param>
        public bool Validate(CreditCard creditCard)
        {
            return true;
        }

        public void NotifyObservers(string args)
        {
            if(ObserverEvent != null)
            {
                ObserverEvent(this, args);
            }
        }
    }
}
