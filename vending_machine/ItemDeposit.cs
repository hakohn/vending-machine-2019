﻿using System;
using System.Collections.Generic;
using System.IO;

namespace vending_machine
{
    public class ItemDeposit
    {
        private List<ContainableItem> containableItems;
        public int Count => containableItems.Count;
        private readonly bool[,] occupiedSpace;


        public ItemDeposit() : this(new Vector2Int(10, 10))
        {
        }
        public ItemDeposit(Vector2Int dimension)
        {
            containableItems = new List<ContainableItem>();

            // Setting up the dimensions of the deposit
            occupiedSpace = new bool[dimension.y, dimension.x];
        }

        /// <summary> Load all the items found within the file. </summary>
        /// <param name="fileName"> The name of the file than countins the required item list information. </param>
        /// <param name="dimension"> The dimensions (rows and columns) of the deposit. </param>
        public void LoadItemsFromFile(string fileName)
        {
            // Adding all the default products
            string[] fileLines = File.ReadAllLines(@fileName);
            foreach (string line in fileLines)
            {
                // Split the read lines by words separated through space and, if positioned correctly, add it to the item collection
                string[] tabSeparated = line.Split(' ');
                Product.ECategory category = (Product.ECategory)Convert.ToByte(tabSeparated[0]);
                float price = (float)Convert.ToDouble(tabSeparated[1]);
                byte size = Convert.ToByte(tabSeparated[2]);
                ushort quantity = Convert.ToUInt16(tabSeparated[3]);
                string name = tabSeparated[4];
                Vector2Int position = new Vector2Int(Convert.ToInt32(tabSeparated[5]), Convert.ToInt32(tabSeparated[6]));


                if (IsSpaceAvailable(position, size))
                {
                    Add(new ContainableItem(category, price, size, quantity, name, position));
                    SetOccupiedSpace(true, position, size);
                }
            }
        }

        /// <summary> Increase the quantity of containableItem by 1, if already within the collection (cannot exceed maximum quantity). 
        /// Else, add the item to the collection </summary>
        /// <param name="item"> The item you wish to add / increase quantity of. </param>
        public void Add(ContainableItem item)
        {
            int index;
            for (index = 0; index < Count; index++)
            {
                if (containableItems[index].Equals(item))
                {
                    if (containableItems[index].Quantity.Current < containableItems[index].Quantity.Maximum)
                    {
                        break;
                    }
                }
            }

            // If so, it means no suitable item stack was found (or was found but with not enough space), so a new one will be added
            if (index >= Count)
            {
                SetOccupiedSpace(true, item.Position, item.Size);
                containableItems.Add(item);
            }
            else // Else, add it to the existing stack and increase its quantity
            {
                containableItems[index].Quantity.Current++;
            }
        }

        /// <summary> Decrease the quantity of containableItem by 1. If the quantity reaches 0 after this, remove the item from the list. </summary>
        /// <param name="item"> The item you wish to remove / decrease quantity of. </param>
        public void Remove(ContainableItem item)
        {
            int index;
            for (index = 0; index < Count; index++)
            {
                if (containableItems[index].Equals(item))
                {
                    break;
                }
            }

            // If the item we're looking for does exist, decrease its quantity. If quantity reaches zero, remove the item from the list.
            if (index < Count)
            {
                containableItems[index].Quantity.Current--;
                if (containableItems[index].Quantity.Current == 0)
                {
                    SetOccupiedSpace(false, containableItems[index].Position, containableItems[index].Size);
                    containableItems.Remove(containableItems[index]);
                }
            }
        }

        /// <param name="itemIndex"> The index of the item you're looking for. </param>
        /// <returns> The item you're looking for, if existing. Else, returns null. </returns>
        public ContainableItem GetItem(int itemIndex)
        {
            if (0 <= itemIndex && itemIndex < Count)
            {
                return containableItems[itemIndex];
            }
            return null;
        }
        
        /// <summary> Get the list of purchasable products as a string. </summary>
        public string GetProductListInformation()
        {
            string productListInformation = string.Empty;
            for (int i = 0; i < Count; i++)
            {
                productListInformation += (i < 10 ? " " : "") + i + ". ";
                productListInformation += GetSpecificProductInformation(i);
                productListInformation += '\n';
            }

            return productListInformation;
        }

        /// <summary> Get the information regarding a certain product as a string. </summary>
        public string GetSpecificProductInformation(int itemIndex)
        {
            string itemInformation = string.Empty;
            var item = GetItem(itemIndex);
            if (item != null)
            {
                itemInformation += item.Name;
                itemInformation += " (" + item.Quantity.Current + "/" + item.Quantity.Maximum + ")";
                itemInformation += " - " + item.Price + " lei";
            }
            else
            {
                itemInformation += "UNKNOWN";
            }

            return itemInformation;
        }

        /// <summary> Check if there is space available at position, for size units. </summary>
        /// <param name="position"> The starting position of the product. </param>
        /// <param name="size"> The size of the product. </param>
        /// <returns> True if the space is available, false otherwise. </returns>
        public bool IsSpaceAvailable(Vector2Int position, int size = 1)
        {
            for (int i = 0; i < size; i++)
                if (occupiedSpace[position.y, position.x + i]) return false;
            return true;
        }

        /// <summary> Set the space as occupied or free somewhere within the deposit. </summary>
        /// <param name="value"> Set the space as occupied? </param>
        /// <param name="position"> The position you want your space editing to begin from. </param>
        /// <param name="size"> The row length you want to edit. </param>
        public void SetOccupiedSpace(bool value, Vector2Int position, int size = 1)
        {
            for (int i = 0; i < size; i++)
                occupiedSpace[position.y, position.x + i] = value;
        }
    }
}
