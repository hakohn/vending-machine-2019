﻿using System;

namespace vending_machine
{
    public class VendingMachine
    {
        private ItemDeposit itemDeposit;
        private Dispenser dispenser;
        private PaymentTerminal paymentTerminal;
        private InputTerminal inputTerminal;
        private Vault vault;
        private DataCollection dataCollection;
        private DisplayScreen display;
        
        public enum EMode { USER, DEV };
        private EMode mode;


        public VendingMachine()
        {
            // Initialize the components
            itemDeposit = new ItemDeposit(new Vector2Int(10, 10));
            dispenser = new Dispenser(itemDeposit);
            paymentTerminal = new PaymentTerminal();
            inputTerminal = new InputTerminal();
            vault = new Vault();
            dataCollection = new DataCollection();
            display = new DisplayScreen(paymentTerminal, itemDeposit, dispenser, dataCollection);

            // Add the observers to the observables
            paymentTerminal.ObserverEvent += display.OnUpdate;

            // Load the items
            itemDeposit.LoadItemsFromFile("items.txt");

            // Add some cash (as coins) to the vault in case change is needed
            vault.CashStack.Add(179.99f, true);
        }

        public void Access()
        {
            display.Stage = DisplayScreen.EStage.SELECT_MODE;
            inputTerminal.GetNumber();
            if(0 <= inputTerminal.NumberInput && inputTerminal.NumberInput < Enum.GetValues(typeof(EMode)).Length)
            {
                mode = (EMode)inputTerminal.NumberInput;
            }

            switch(mode)
            {
                case EMode.USER:
                    while (true)
                    {
                        CashStack userCash = new CashStack(1000);
                        CreditCard userCredit = new CreditCard(1000);

                        display.Stage = DisplayScreen.EStage.SELECT_PRODUCT;
                        // Select the item you wish to purchase
                        int orderIndex = inputTerminal.GetNumber();
                        if (0 <= orderIndex && orderIndex < itemDeposit.Count)
                        {
                            // Select the payment method
                            display.Stage = DisplayScreen.EStage.SELECT_PAYMENT_METHOD;
                            do
                            {
                                inputTerminal.GetNumber();
                            } while (!(0 <= inputTerminal.NumberInput && inputTerminal.NumberInput < Enum.GetValues(typeof(PaymentTerminal.EPayMethod)).Length));
                            paymentTerminal.PayMethod = (PaymentTerminal.EPayMethod)inputTerminal.NumberInput;

                            paymentTerminal.AmountRequired = itemDeposit.GetItem(orderIndex).Price;
                            display.Stage = DisplayScreen.EStage.PAYMENT_PROCESS;

                            switch (paymentTerminal.PayMethod)
                            {
                                case PaymentTerminal.EPayMethod.CASH:
                                    CashStack currentCashInserted = new CashStack();

                                    // Wait until all the required money was inserted
                                    while (paymentTerminal.PaymentInProcess)
                                    {
                                        inputTerminal.GetCash();
                                        double cashValue = inputTerminal.InsertedCashValue;
                                        if (currentCashInserted.Insert(cashValue) == false)
                                        {
                                            continue;
                                        }

                                        userCash.Remove(cashValue);
                                        paymentTerminal.CollectMoney(cashValue);
                                    }

                                    // Give back the change (in coins)
                                    double change = currentCashInserted.Value - paymentTerminal.AmountRequired;
                                    if (vault.CashStack.Remove(change, true) == true)
                                    {
                                        userCash.Add(change, true);
                                    }
                                    break;

                                case PaymentTerminal.EPayMethod.CREDIT:
                                    // Before payment, check if the credit card is valid
                                    if (paymentTerminal.Validate(userCredit) == true)
                                    {
                                        // Check if there is enough money on the credit card
                                        if (userCredit.Remove(paymentTerminal.AmountRequired) == true)
                                        {
                                            vault.Credit.Add(paymentTerminal.AmountRequired);
                                            paymentTerminal.CollectMoney(paymentTerminal.AmountRequired);
                                        }
                                    }
                                    break;
                            }

                            // Deliver the item to the user
                            dispenser.DeliverItem(orderIndex);
                            dataCollection.AddSoldProduct(itemDeposit.GetItem(orderIndex));
                            display.Stage = DisplayScreen.EStage.AFTER_PAYMENT;
                        }
                    }
                    break;




                case EMode.DEV:
                    display.Stage = DisplayScreen.EStage.DEV_DATA;
                    while(true) { }

                    break;
            }
        }
    }
}
