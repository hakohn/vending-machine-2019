﻿namespace vending_machine
{
    public class CreditCard : IMoneyCollection
    {
        public enum EIssuingNetwork { UNKNOWN, VISA, MASTERCARD, AMERICAN_EXPRESS }
        public EIssuingNetwork IssuingNetwork { get; private set; }
        public string Number { get; private set; }
        public string CCV { get; private set; }
        public class CExpirationDate
        {
            private int _month;
            public int Month
            {
                get => _month;
                set
                {
                    if(1 <= value && value <= 12)
                    {
                        _month = value;
                    }
                }
            }
            private int _year;
            public int Year
            {
                get => _year;
                set
                {
                    if(0 <= value && value <= 99)
                    {
                        _year = value;
                    }
                }
            }

            private string ConvertToString(int value)
            {
                return (0 <= value && value <= 9) ? ("0" + value.ToString()) : value.ToString();
            }
            public string String => ConvertToString(Month) + "/" + ConvertToString(Year);
        }
        public CExpirationDate ExpirationDate { get; private set; }
        public string CardHolderName { get; private set; }

        /// <summary> The amount of money present on the credit card. </summary>
        public double Value
        {
            get;
            private set;
        }

        public CreditCard() : this(0)
        {
        }
        public CreditCard(float value) : this(value, EIssuingNetwork.UNKNOWN, "Unknown", "Unknown", null, "Unknown")
        {
            Value = value;
        }
        public CreditCard(float value, EIssuingNetwork issuingNetwork, string number, string ccv, CExpirationDate expirationDate, string cardHolderName)
        {
            IssuingNetwork = issuingNetwork;
            Number = number;
            CCV = ccv;
            ExpirationDate = expirationDate;
            CardHolderName = cardHolderName;
        }

        /// <summary> Add a certain amount of money to the credit card. </summary>
        /// <param name="value"> The amount of money you want to add. </param>
        public void Add(double value)
        {
            Value += value;
        }

        /// <summary> Remove a certain amount of money from the credit card. </summary>
        /// <param name="value"> The amount of money you want to remove. </param>
        /// <returns> True if the value has been withdrawn, false otherwise. </returns>
        public bool Remove(double value)
        {
            if (value > Value)
            {
                return false;
            }

            Value -= value;
            return true;
        }
    }
}
