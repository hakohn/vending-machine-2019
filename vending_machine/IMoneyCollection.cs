﻿namespace vending_machine
{
    public interface IMoneyCollection
    {
        double Value
        {
            get;
        }

        void Add(double value);
        bool Remove(double value);
    }
}
