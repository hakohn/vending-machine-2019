﻿using System;
using System.Collections.Generic;

namespace vending_machine
{
    public class InputTerminal
    {
        /// <summary> The value of the last number that has been typed in. </summary>
        public int NumberInput
        {
            get;
            private set;
        }
        /// <summary> The value of the last coin / banknote that has been inserted. </summary>
        public double InsertedCashValue
        {
            get;
            private set;
        }

        public double GetCash()
        {
            string input = Console.ReadLine();
            InsertedCashValue = Convert.ToDouble(string.IsNullOrEmpty(input) ? "-1" : input);
            return InsertedCashValue;
        }
        public int GetNumber()
        {
            string input = Console.ReadLine();
            NumberInput = Convert.ToInt32(string.IsNullOrEmpty(input) ? "-1" : input);
            return NumberInput;
        }
    }
}
