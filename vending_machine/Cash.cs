﻿using System;

namespace vending_machine
{
    public class Cash : IEquatable<Cash>
    {
        public enum EType
        {
            _1_BAN, _5_BANI, _10_BANI, _50_BANI,
            _1_LEU, _5_LEI, _10_LEI, _50_LEI, _100_LEI, _200_LEI, _500_LEI
        }
        public readonly EType Type;
        public uint Quantity;
        public float Value
        {
            get
            {
                switch (Type)
                {
                    case EType._1_BAN: return 0.01f;
                    case EType._5_BANI: return 0.05f;
                    case EType._10_BANI: return 0.10f;
                    case EType._50_BANI: return 0.50f;

                    case EType._1_LEU: return 1;
                    case EType._5_LEI: return 5;
                    case EType._10_LEI: return 10;
                    case EType._50_LEI: return 50;
                    case EType._100_LEI: return 100;
                    case EType._200_LEI: return 200;
                    case EType._500_LEI: return 500;

                    default: return 0;
                }
            }
        }
        public bool IsCoin
        {
            get
            {
                return Value < 1f ? true : false;
            }
        }

        public Cash(EType cashType)
        {
            Type = cashType;
        }

        public bool Equals(Cash other)
        {
            if (Type == other.Type)
            {
                return true;
            }

            return false;
        }
    }
}
