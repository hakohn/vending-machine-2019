﻿namespace vending_machine
{
    public class Vault
    {
        public CashStack CashStack { get; set; }
        public CreditCard Credit { get; set; }

        public Vault()
        {
            CashStack = new CashStack();
            Credit = new CreditCard();
        }
    }
}
