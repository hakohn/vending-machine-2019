﻿namespace vending_machine
{
    public class Dispenser
    {
        private ItemDeposit itemDeposit;
        public ContainableItem LastDispensedItem
        {
            get;
            private set;
        }
        public bool DeliverSuccess
        {
            get;
            private set;
        }


        public Dispenser(ItemDeposit itemDeposit)
        {
            this.itemDeposit = itemDeposit;
        }

        public void DeliverItem(int itemIndex)
        {
            var item = itemDeposit.GetItem(itemIndex);
            if (item != null)
            {
                LastDispensedItem = item;
                DeliverSuccess = true;
                itemDeposit.Remove(item);
            }
            else
            {
                DeliverSuccess = false;
            }
        }
    }
}
