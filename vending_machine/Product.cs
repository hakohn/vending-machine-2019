﻿using System;

namespace vending_machine
{
    public class Product : IEquatable<Product>
    {
        public enum ECategory { UNKNOWN, SODA, SNACKS, WATER };
        public struct SQuantity
        {
            private ushort current;
            public ushort Current
            {
                get => current;
                set
                {
                    if (0 <= value && value <= Maximum)
                    {
                        current = value;
                    }
                }
            }
            public ushort Maximum;
        }

        public readonly ECategory Category;
        public float Price { get; private set; }
        public readonly byte Size;
        public SQuantity Quantity = new SQuantity();
        public readonly string Name;

        public Product(ECategory category, float price, byte size, ushort quantity, string name)
        {
            Category = category;
            Price = price;
            Size = size;
            Quantity.Maximum = quantity;
            Quantity.Current = quantity;
            Name = name;
        }

        public bool Equals(Product other)
        {
            if (other.Category == Category && other.Size == Size && other.Name == Name)
            {
                return true;
            }

            return false;
        }
    }
}
