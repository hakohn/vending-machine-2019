﻿namespace vending_machine
{
    public class ContainableItem : Product
    {
        public readonly Vector2Int Position;

        public ContainableItem(ECategory category, float price, byte size, ushort quantity, string name, Vector2Int position) : base(category, price, size, quantity, name)
        {
            Position = position;
        }
    }
}
