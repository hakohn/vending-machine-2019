﻿using System;
using System.Diagnostics;

namespace vending_machine
{
    public class DisplayScreen
    {
        // Observer
        private PaymentTerminal paymentTerminal;
        private ItemDeposit itemDeposit;
        private Dispenser dispenser;
        private DataCollection dataCollection;

        public enum EStage {
            UNKNOWN, SELECT_MODE, // General stages
            SELECT_PRODUCT, SELECT_PAYMENT_METHOD, PAYMENT_PROCESS, AFTER_PAYMENT, // User-specific stages
            DEV_DATA // Dev-specific stages
        }
        private EStage _stage;
        public EStage Stage
        {
            get => _stage;
            set
            {
                _stage = value;
                OnUpdate(this, string.Empty);
            }
        }
        private Stopwatch stopwatch;
        
        public readonly ConsoleColor defaultTextColor;

        /// <summary> The sources of the display information. </summary>
        public DisplayScreen(PaymentTerminal paymentTerminal, ItemDeposit itemDeposit, Dispenser dispenser, DataCollection dataCollection)
        {
            // Initialize the references
            this.paymentTerminal = paymentTerminal;
            this.itemDeposit = itemDeposit;
            this.dispenser = dispenser;
            this.dataCollection = dataCollection;

            // Initialize the private varaibles
            stopwatch = new Stopwatch();
            stopwatch.Stop();

            // Initialize the readonly variables
            defaultTextColor = ConsoleColor.DarkCyan;
        }

        /// <summary> Draw the string on the screen. </summary>
        /// <param name="value"> The text string. </param>
        public void Draw(string value)
        {
            Draw(value, defaultTextColor);
        }
        /// <param name="textColor"> The foreground text color. </param>
        public void Draw(string value, ConsoleColor textColor)
        {
            Console.ForegroundColor = textColor;
            Console.Write(value);
        }

        /// <summary> Clears all the text from the screen. </summary>
        public void ClearScreen()
        {
            Console.Clear();
        }

        public void OnUpdate(object sender, string args)
        {
            ClearScreen();
            switch(Stage)
            {
                case EStage.SELECT_MODE:
                    Draw("Are you a user, or a developer?\n");
                    Draw("0. User\n", ConsoleColor.DarkGreen);
                    Draw("1. Developer\n", ConsoleColor.DarkMagenta);
                    break;

                case EStage.SELECT_PRODUCT:
                    Draw("The currently purchasable items are:\n");
                    Draw(itemDeposit.GetProductListInformation(), ConsoleColor.DarkYellow);
                    Draw("\nThe index of the item you wish to buy: ");
                    break;


                case EStage.SELECT_PAYMENT_METHOD:
                    Draw("Select the payment method:\n");
                    Draw(paymentTerminal.GetPaymentMethodListInformation(), ConsoleColor.DarkYellow);
                    Draw("\nThe index of the payment method: ");
                    break;


                case EStage.PAYMENT_PROCESS:
                    Draw(paymentTerminal.AmountRequired + " lei are still required\n");
                    Draw("The value (in lei) of the coin / banknote you want to introduce: ");
                    break;


                case EStage.AFTER_PAYMENT:
                    if(dispenser.DeliverSuccess)
                    {
                        Draw("Success! " + dispenser.LastDispensedItem.Name + " has been delivered!", ConsoleColor.DarkGreen);
                    }
                    else
                    {
                        Draw("Unable to deliver " + dispenser.LastDispensedItem.Name + "!", ConsoleColor.Red);
                    }

                    stopwatch.Start();
                    while (stopwatch.Elapsed.Seconds < 3) { }
                    stopwatch.Reset();
                    stopwatch.Stop();
                    break;

                case EStage.DEV_DATA:
                    foreach(string line in dataCollection.GetProductSalesInformation())
                    {
                        Draw(line + "\n");
                    }
                    break;
            }
        }
    }
}
